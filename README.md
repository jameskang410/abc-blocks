Question
--------
You are encouraged to solve this task according to the task description, using any language you may know.
You are given a collection of ABC blocks. Just like the ones you had when you were a kid. The sample blocks are:

((B, O),
(D, O),
(G, T))

The goal of this task is to write a function that takes a string and a collection of blocks and determine whether you can spell the word with the blocks or not. 

The rule is simple: Once a letter on a block is used that block cannot be used again
~~Assume each block contains at least one letter from the word~~ (**EDIT: can take in blocks that will not be used**).

Example word that can be spelled: Dog, Too

Example word that cannot be spelled: Boo


How to use the CLI
------------------
* Build the jar using the gradle jar task
* Run the jar using this example: `java -jar abc.jar boo bo do gt`
* Possible responses include:
    * `false` : cannot create string using the given 2 letter blocks
    * `<blocks used to create string> true` : string can be successfully created using the array of blocks that is printed