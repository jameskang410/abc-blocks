package com.rallyhealth;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests for Main class - specifically focused on abcBlocks()
 */
public class TestMain {

    @Test
    public void testAbcBlocksTrueCaseBlocksInOrder() {
        char[][] input = {{'B','O'}, {'D', 'O'}, {'T', 'G'}};
        assertTrue(Main.abcBlocks("BOT", input));
    }

    @Test
    public void testAbcBlocksTrueCaseBlocksNotInOrder() {
        char[][] input = {{'B','O'}, {'D', 'O'}, {'T', 'G'}};
        assertTrue(Main.abcBlocks("DOG", input));
    }

    @Test
    public void testAbcBlocksLettersFromSameBlock() {
        // 'T' and 'G' are from the same block
        char[][] input = {{'B','O'}, {'D', 'O'}, {'T', 'G'}};
        assertFalse(Main.abcBlocks("GOT", input));
    }

    @Test
    public void testAbcBlocksLetterNotInAnyBlock() {
        // No 'E' in any block
        char[][] input = {{'B','O'}, {'D', 'O'}, {'T', 'G'}};
        assertFalse(Main.abcBlocks("OOE", input));
    }
}
