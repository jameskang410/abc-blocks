package com.rallyhealth;

import java.util.Arrays;

public class Main {
    public static boolean abcBlocks(String word, char[][] blocks) {
        return abcBlocks(word, blocks, 0);
    }

    public static boolean abcBlocks(String word, char[][] blocks, int index) {
        // stopping condition: made it through whole word
        if (index == word.length()) {

            // for the purpose of printing solution
            char[][] solutionBlocks = Arrays.copyOfRange(blocks, 0, index);
            System.out.println(Arrays.deepToString(solutionBlocks));

            return true;
        }

        char letter = word.charAt(index);

        for (int i = index; i < blocks.length; i++) {
            char[] block = blocks[i];

            // this block doesn't have the correct letter -> skip
            if (block[0] != letter && block[1] != letter) {
                continue;
            }

            // add
            swap(blocks, index, i);

            // recurse
            //   - moving onto next letter in word
            //   - blocks is ordered bc of successful swap, so moving onto next block
            if (abcBlocks(word, blocks, index + 1)) {
                return true;
            }

            // backtrack
            swap(blocks, index, i);
        }

        return false;
    }

    private static void swap(char[][] blocks, int blockIndex1, int blockIndex2) {
        char[] temp = blocks[blockIndex1];
        blocks[blockIndex1] = blocks[blockIndex2];
        blocks[blockIndex2] = temp;
    }

    private static char[] createBlock(String letters) {
        return new char[] {Character.toUpperCase(letters.charAt(0)), Character.toUpperCase(letters.charAt(1))};
    }

    public static void main(String[] args){
        if (args.length < 2) {
            System.out.println("ERROR: First argument must be a string. Following arguments must be 2 letter strings");
            return;
        }

        String word = args[0];
        char[][] blocks = new char[args.length - 1][];

        for (int i = 1; i < args.length; i++) {
            if (args[i].length() != 2) {
                System.out.println("ERROR: Arguments representing blocks must be 2 letter strings");
                return;
            }

            blocks[i - 1] = createBlock(args[i]);
        }

        System.out.println(abcBlocks(word.toUpperCase(), blocks));
    }
}
